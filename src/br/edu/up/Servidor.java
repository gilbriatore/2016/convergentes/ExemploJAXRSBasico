package br.edu.up;

import java.net.InetSocketAddress;

import javax.ws.rs.ext.RuntimeDelegate;
import com.sun.net.httpserver.HttpServer; //Classes restritas
import com.sun.net.httpserver.HttpHandler; //Classes restritas

@SuppressWarnings("restriction")
public class Servidor {

	public static void main(String[] args) throws Exception {

		// Cria o socket e o server;
		InetSocketAddress isa = new InetSocketAddress(9090);
		HttpServer server = HttpServer.create(isa, 0);

		// Cria o web service e o handler;
		WebServices ws = new WebServices();
		HttpHandler handler = RuntimeDelegate.getInstance().createEndpoint(ws, HttpHandler.class);

		// Vincula o handler e inicia o servidor;
		server.createContext("/", handler);
		server.start();

		System.out.println("http://localhost:9090/ws rodando...");
	}
}